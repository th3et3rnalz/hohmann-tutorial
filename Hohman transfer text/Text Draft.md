# Hohmann Transfer

An engineer from SpaceCorp has just received the assignment to find the best thruster to move a communications satellite from a Low Earth Orbit (300 km altitude) to a Geostationary orbit (35 800 km altitude). Luckily, with her knowledge of Hohmann transfers and Satsearch's API, Hannah can do this.

Let's start with what a Hohman transfer really is: the most efficient way to change the orbit of a spacecraft.

<img src="orbitchange.png">

It's been a while since Hannah did calculations regarding Hohmann transfers, so she does some research and comes up with the following:

* At least one burn is required to change from the initial orbit (Low Earth Orbit in this case), to a transfer orbit.
* Once the spacecraft is in the transfer orbit, it must wait half a rotation period, and then fire it's thrusters again (for a different amount of time) to enter the final orbit.
* The velocity of a spacecraft is given by:

$$
\begin{align}
Circular \;orbit\quad:&\qquad v_{orbit}= \sqrt{\frac{M_{e}G}{R_e + h}}\\
Elliptial\;orbit\quad:&\qquad v_{orbit}= \sqrt{2*GM_e \left( \frac{1}{R_e+h} - \frac{1}{2a} \right)}

\end{align}
$$

Where a the semi-major axis, G is the universal gravitational constant, $M_E$ is the mass of the earth, $R_e$ is the radius of the earth, and h is the altitude of the spacecraft. The semi-major axis of an orbit is calculated as follows:
$$
a = \frac{r_{a} + r_p}{2}
$$
<img src="elliptical.jpeg">

So, let's start the calculations.

But how do we get from one orbit to another ? More specifically, how does a spacecraft go from a circular orbit in LEO to a circular orbit in GEO? Well, like this:

| Maneouvre number | What happens                                                 |
| ---------------- | ------------------------------------------------------------ |
| 1                | Change from circular orbit at 300 km (Low earth orbit), to elliptical transfer orbit with perigee (shortest distance to earth) altitude of 300 km and apogee (largest distance to earth)altitude of 35786 km from earth. |
| 2                | Change from the elliptical transfer orbit to a circular geostationary orbit (at 35786 km altitude). |

#### First orbit change

To know what change in velocity is required to move from LEO (300 km) to a transfer orbit between LEO and GEO, we must first know the orbital velocity of the spacecraft at LEO:
$$
V_{orbit} =  \sqrt{\frac{M_{e}G}{R_e + h}} =  \sqrt{\frac{5.972*10^{24}*6.674*10^{-11}}{6378*10^3 + 300*10^3}}=7725.6 \;\frac{m}{s}  = 7.73 \;\frac{km}{s}
$$
Let's then find the speed that the spacecraft should have in the transfer orbit when it is at a distance of 300 km from Earth (the perigee of the elliptical orbit). The semi-major axis for this orbit is:
$$
a = \frac{(300+6378)*10^{3} + (35786 + 6378)810^3}{2} = 24421000\;m = 2.44*10^7 m
$$
We can then find the velocity:
$$
\begin{align}
v_{orbit}&= \sqrt{2*GM_e \left( \frac{1}{R_e+h} - \frac{1}{2a} \right)}\\
&= \sqrt{2*5.972*10^{24}*6.674*10^{-11} \left( \frac{1}{(6378+300)*10^3} - \frac{1}{2 * 2.44*10^7} \right)} = 10151.3 \; \frac{m}{s}\\
&= 10.15 \; \frac{km}{s}
\end{align}
$$
The increase in velocity the has to be given by the thruster is thus 
$$
\Delta V = 10.15-7.73 = 2.42 \; \frac{km}{s} 
$$

#### Second orbit change

Now that the spacecraft is in its elliptical orbit, its distance changes over time. Half an orbit after the thrusters were fired, it is at is maximum distance from earth. We can calculate its velocity at that point:
$$
\begin{align}
v_{orbit}&= \sqrt{2*GM_e \left( \frac{1}{R_e+h} - \frac{1}{2a} \right)}\\
&= \sqrt{2*5.972*10^{24}*6.674*10^{-11} \left( \frac{1}{(6378+35786)*10^3} - \frac{1}{2 * 2.44*10^7} \right)} = 1607.78 \; \frac{m}{s}\\
&= 1.61 \; \frac{km}{s}
\end{align}
$$
The velocity that the spacecraft needs to have, for it to be in a stable circular orbit at an altitude of 35786 km can also be calculated:
$$
V_{orbit} =  \sqrt{\frac{M_{e}G}{R_e + h}} =  \sqrt{\frac{5.972*10^{24}*6.674*10^{-11}}{6378*10^3 + 35786*10^3}}=3074.57 \;\frac{m}{s}  = 3.07 \;\frac{km}{s}
$$
The change in velocity for the second manoeuvre is then:
$$
\Delta V = 3.07 - 2.42 = 0.65 \; \frac{km}{s} = 650 \; \frac{m}{s}
$$
The total change in speed for both maneouvres is then:
$$
\Delta V_{total} = 2.42 + 0.65 = 3.07 \; \frac{km}{s}
$$

#### What is al this $\Delta V$ business?

The $\Delta V$'s mentioned earlier are the changes in velocity that the space craft must undergo in order to change orbit(s). Now that we have these velocities, we can calculate the propellant mass required, with the rocket equation:
$$
\Delta V = I_{sp} \;g\; ln\left(\frac{M_{start}}{M_{end}} \right)
$$
Where g is the gravitational constant, $M_{start}$  is the mass before the engines are lit and $M_{end}$ is the mass after the fuel tanks are depleted. But what is this $I_sp$? Well, that is the specific impulse, a measure of the efficiency of an engine. Since this is engine specific, Hannah will want to repeat the following calculations for multiple engines, luckily, with Satsearch's API and a bit of python she can do just that. 

## Python and Jupyter notebook

If you don't have Jupyter installed, [you can follow this tutorial](https://jupyter.org/install "Official installation Jupyter").  