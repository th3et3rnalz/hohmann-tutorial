from PySatsearch import Satsearch
from dotenv import load_dotenv
import os


load_dotenv('.env')
category_uuid = "a7bb0edc-4b93-516c-a0fd-878d69f1f580"


satsearch = Satsearch(os.getenv('APP_ID'), os.getenv('API_TOKEN'))

# I THINK THIS IS WHAT YOU'LL WANT TO SHOW
products = satsearch.get_all_prodcuts(category_uuid)

# Possibly simplify:
products = satsearch.get_all_prodcuts(category_uuid, simple=True)

